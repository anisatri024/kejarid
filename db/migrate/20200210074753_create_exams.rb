class CreateExams < ActiveRecord::Migration[6.0]
  def up
    create_table :exams do |t|
      t.string :title ,default:'belum ada judul', limit:100
      t.string :mapel ,default: 'Mapel' , limit:100
      t.string :duration ,default: 'duration', limit:100
      t.float :nilai ,default:0
      t.string :status ,default:''
      t.string :level ,default:''
      t.string :student_id       
      
      t.timestamps
    end
  end
  def down 
    drop_table :exams
end
end