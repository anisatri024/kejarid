# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Payment.create(id_transaction:'111', status:'aktif', upload:'12-02-2020')
Payment.create(id_transaction:'112', status:'aktif', upload:'12-02-2020')
Payment.create(id_transaction:'113', status:'nonaktifkan', upload:'12-02-2020')
Payment.create(id_transaction:'114', status:'nonaktifkan', upload:'12-02-2020')
Payment.create(id_transaction:'115', status:'aktif', upload:'12-02-2020')
Payment.create(id_transaction:'116', status:'aktif', upload:'12-02-2020')