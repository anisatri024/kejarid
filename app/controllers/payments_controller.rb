class PaymentsController < ApplicationController
  def new #untuk menampilkan form data baru
    @payment = Payment.new
  end  
    
  def create #untuk memperoses data baru yang dimasukkan di form new
    payment = Payment.new (resources_params)
    payment.save 
    redirect_to payments_path
    flash[:notice] = 'Payment has been created'
  end  
    
  def edit #menampilkan data yang sudah disimpan
    @payment = Payment.find(params[:id])
  end 

  def update #melakukan proses ketika user mengedit data
    @payment = Payment.find(params[:id])
    @payment.update(resources_params)
    flash[:notice] = 'Payment has been update'
    redirect_to payments_path(@payment)
  end  

  def destroy #untuk menghapus data
    @payment = Payment.find(params[:id])
    @payment.destroy
    flash[:notice] = 'Payment has been destroy'
    redirect_to payments_path(@payment)
  end 

  def index #menampilkan seluruh data yang ada di database
    @payments = Payment.all
  end 

  def show #menampilkan  sebuah data sectemplateara detail
      id = params[:id]
      @payment = Payment.find(id)
      # render plain: id
      # render plain: @payment.title 
     end

    private 

  def resources_params
    params.required(:payment).permit(:id_transaction, :status, :upload)
  end
  end